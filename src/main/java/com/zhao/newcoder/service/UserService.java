package com.zhao.newcoder.service;

import com.zhao.newcoder.DTO.User;
import com.zhao.newcoder.DTO.response.UserResponse;

import java.util.List;

/**
 * 用户接口
 *
 * @author: ZhouBin
 * @date: 2020/10/9 9:16 上午
 */
public interface UserService {
    /**
     * TODO
     * 用户注册
     *
     * @author: ZhouBin
     * @date: 2020/10/9 9:47 上午
     * @param user
     * @return void
     */
    void insertUser(User user);

    /**
     * TODO
     * 用户信息修改
     *
     * @author: ZhouBin
     * @date: 2020/10/9 9:47 上午
     * @param user
     * @return void
     */
    void updateUser(User user);

    /**
     * TODO
     * 用户信息删除
     *
     * @author: ZhouBin
     * @date: 2020/10/9 9:47 上午
     * @param id
     * @return void
     */
    void deleteUser(String id);

    /**
     * 根据id查询用户信息
     *
     * @author: ZhouBin
     * @date: 2020/10/9 9:23 上午
     * @param id
     * @return com.zhao.newcoder.DTO.User
     */
    User findById(String id);

    /**
     * TODO
     * 接口目前查询的所有的用户信息，没有加筛选条件
     *
     * @author: ZhouBin
     * @date: 2020/10/9 9:24 上午
     * @param user
     * @return java.util.List<com.zhao.newcoder.DTO.User>
     */
    List<User> findAll(User user);

    /**
     * TODO
     * 目前没有登录逻辑，仅在session中保存了userResponse信息
     *
     * @author: ZhouBin
     * @date: 2020/10/9 7:21 下午
     * @param userResponse
     * @return com.zhao.newcoder.DTO.response.UserResponse
     */
    UserResponse login(UserResponse userResponse);
}
