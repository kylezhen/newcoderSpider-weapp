package com.zhao.newcoder.Repository;

import java.util.List;

import com.zhao.newcoder.DTO.AuthUser;
import com.zhao.newcoder.VO.LoginResult;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthUserRepository extends JpaRepository<AuthUser, Long> {
    // 通过手机号查询
    List findByMobile(String mobile);

    AuthUser findByMobileAndPassword(String mobile, String password);
}