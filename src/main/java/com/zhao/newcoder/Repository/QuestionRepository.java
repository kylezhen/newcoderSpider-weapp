package com.zhao.newcoder.Repository;

import com.zhao.newcoder.DTO.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestionRepository  extends JpaRepository<Question,Long> {

    @Query(value = "select distinct type from question",nativeQuery = true)
    List<String> queryType();
    
}
