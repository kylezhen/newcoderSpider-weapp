package com.zhao.newcoder.Repository;

import com.zhao.newcoder.VO.ESCoderVo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CoderRepository extends ElasticsearchRepository<ESCoderVo,Long> {
}
