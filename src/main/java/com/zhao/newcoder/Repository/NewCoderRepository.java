package com.zhao.newcoder.Repository;

import com.zhao.newcoder.DTO.NewCoderDTO;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NewCoderRepository extends JpaRepository<NewCoderDTO,Long> {
}
