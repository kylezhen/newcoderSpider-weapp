package com.zhao.newcoder.Repository;

import com.zhao.newcoder.DTO.Img;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgRepository extends JpaRepository<Img, Integer> {
}
