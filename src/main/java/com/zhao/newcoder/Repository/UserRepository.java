package com.zhao.newcoder.Repository;

import com.zhao.newcoder.DTO.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,String>, JpaSpecificationExecutor<User> {

   public User findUserByOpenid(String openid);

   //public  User findUserByOpenidOrId(String openid,String id);
}
