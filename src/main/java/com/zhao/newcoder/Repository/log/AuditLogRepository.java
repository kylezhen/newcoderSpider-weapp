package com.zhao.newcoder.Repository.log;

import com.zhao.newcoder.DTO.log.AuditLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * auditlog的repository
 *
 * @author: ZhouBin
 * @date: 2020/10/9 12:01 下午
 */
@Repository
public interface AuditLogRepository extends JpaRepository<AuditLog,Long>, JpaSpecificationExecutor<AuditLog> {
}
