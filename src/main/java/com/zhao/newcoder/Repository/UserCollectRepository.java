package com.zhao.newcoder.Repository;

import com.zhao.newcoder.DTO.UserCollectDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCollectRepository extends JpaRepository<UserCollectDTO,String> {

    public UserCollectDTO findByUserIdAndAndInterviewId(String userid,Long interviewId);


}
