package com.zhao.newcoder.exception;

import com.zhao.newcoder.Soup.Result;
import lombok.Data;

/**
 * 通用异常处理
 *
 * @author: ZhouBin
 * @date: 2020/10/2 5:35 下午
 */
@Data
public class CommonException extends Exception{

    private Result result;

    public CommonException(Result result) {
        this.result = result;
    }

    public CommonException(String message, Result result) {
        super(message);
        this.result = result;
    }
}
