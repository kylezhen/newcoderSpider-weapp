package com.zhao.newcoder.controller.base;

import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 基础的controller类
 *
 * @author: ZhouBin
 * @date: 2020/10/4 9:30 上午
 */
public class BaseController {

    public HttpServletRequest request;
    public HttpServletResponse response;


    @ModelAttribute
    public void setResAnReq(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    }
}
