package com.zhao.newcoder.controller;

// import com.zhao.newcoder.DTO.NewCoderDTO;
import com.zhao.newcoder.DTO.Question;
import com.zhao.newcoder.Repository.QuestionRepository;
// import com.zhao.newcoder.VO.NewCoderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
// import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.zhao.newcoder.Soup.Result;

import java.util.List;


@Api(value = "问答题接口", tags = "问答题相关接口")
@RestController
@CrossOrigin
public class QuestionController {

    @Autowired
    QuestionRepository questionRepository;


    @GetMapping("question")
    @ApiOperation(value = "问题列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页码"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量")

    })
    public Page<Question> questionPage(@RequestParam(name = "pageNo",required = false) Integer pageNo,
                                       @RequestParam(name = "pageSize",required = false)Integer pageSize,
                                       @RequestParam(name = "type",required = false)String type){
        if (pageNo == null){
            pageNo = 1;
        }
        if (pageSize==null){
            pageSize =10;
        }
        Question dto = new Question();
        dto.setType(type);
        Example<Question> example=Example.of(dto);


        Sort sort = Sort.by(Sort.Direction.DESC,"createDt"); // 这里的"recordNo"是实体类的主键，记住一定要是实体类的属性，而不能是数据库的字段
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort); // （当前页， 每页记录数， 排序方式）
        //Page<Question> list =questionRepository.findAll(pageable);
        Page<Question> list = questionRepository.findAll(example,pageable);
        return list;
    }
    
    @GetMapping("questionType")
    @ApiOperation(value = "问题类型")
    public List<String> type(){
        List<String> type =questionRepository.queryType();
        return type;
    }

    @PostMapping("question")
    @ApiOperation(value = "新增题目")
    public Result<Question> SaveQuestion(@RequestBody Question dto) {
        dto.setAnswer(dto.answer);
        dto.setTypeTitle(dto.typeTitle);
        dto.setCreateDt(dto.createDt);
        dto.setQuestion(dto.question);
        dto.setType(dto.type);
        // dto.setId((long) 111111);
        dto.setAnswerTxt(dto.answerTxt);
        // return questionRepository.save(dto);
        return new Result(200,"保存成功",dto);

        //  return QuestionRepository.save(dto)
        // return new Result(200,"保存成功", dto)
    }
}
