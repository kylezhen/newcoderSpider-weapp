package com.zhao.newcoder.controller;

import javax.servlet.http.HttpServletRequest;
import com.zhao.newcoder.DTO.AuthUser;
import com.zhao.newcoder.VO.LoginResult;
// import com.zhao.newcoder.service.LoginService;
import com.zhao.newcoder.service.LoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
// import service.LoginService;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
// import io.swagger.annotations.Api;

@RequestMapping("passport")
@CrossOrigin
@RestController
@Api(value = "登陆相关接口", tags = "登陆相关接口")
public class PassportController {
	@Autowired
	private LoginService loginService;

	@PostMapping("login")
	@ResponseBody
	public LoginResult login(@RequestBody AuthUser authUserVO,  HttpServletRequest request) {
		LoginResult loginResult = new LoginResult();
		AuthUser authUser = loginService.login(authUserVO.getMobile(), authUserVO.getPassword());
		// request.sess("admin", loginResult);
		if (authUser != null) {
			request.getSession().setAttribute("admin", authUser);
			loginResult.setCode("200");
			loginResult.setCodeDesc("登陆成功");
			loginResult.setUserId(authUser.getId());
			loginResult.setUsername(authUser.getMobile());
		} else {
			loginResult.setCode("401");
			loginResult.setCodeDesc("账号或密码错误");
		}
		return loginResult;
	}

	@RequestMapping("register")
	@ResponseBody
	public LoginResult register(String username, String password, Integer accountType) {
		LoginResult loginResult = loginService.register(username, password, accountType);
		return loginResult;
	}
}