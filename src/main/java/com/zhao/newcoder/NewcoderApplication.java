package com.zhao.newcoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class NewcoderApplication {

    // 主方法
    public static void main(String[] args) {
        SpringApplication.run(NewcoderApplication.class, args);
    }

}
