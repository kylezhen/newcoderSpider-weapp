package com.zhao.newcoder.utils;

import org.springframework.util.DigestUtils;

/** 
* @author 夏志勇(xzy406728963@163.com)
* @version 创建时间：2020-9-20
* @deprecated 用户输入账号密码的MD5加密
*/
public class MD5Util {
	public static String md5(String src) {
		return DigestUtils.md5DigestAsHex(src.getBytes());
	}
}