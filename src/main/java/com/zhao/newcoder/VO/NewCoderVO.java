package com.zhao.newcoder.VO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Data
@Getter
@Setter
@NoArgsConstructor
public class NewCoderVO  {
    @Id
    @ApiModelProperty(value = "面经id")
    private  Long id;

    @ApiModelProperty(value = "标题")
    private  String title;

    @Field(type = FieldType.Keyword,analyzer = "ik_max_word")
    @ApiModelProperty(value = "摘要")
    private  String  brief;
    @ApiModelProperty(value = "面经标签")
    private  String  label;
    @ApiModelProperty(value = "面经内容")
    private  String content;
    @ApiModelProperty(value = "面经创建时间")
    private  String createTime;
    @ApiModelProperty(value = "作者")
    private  String author;
    @ApiModelProperty(value = "面经真实url")
    private  String  realUrl;


    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    @ApiModelProperty(value = "面经纯文字内容，后续用于全文检索")
    private  String txt;
    @ApiModelProperty(value = "面经类型，例如Java,iOS等")
    private  String type;
    @ApiModelProperty(value = "后续可能会用来区分校招实习，社招类型")
    private  Integer phaseId;
    @ApiModelProperty(value = "是否收藏，true,收藏，false未收藏")
    private  Boolean isCollect;
    @ApiModelProperty(value = "背景图片url地址")
    private String image_url;
    @ApiModelProperty("浏览量")
    private Long viewNum;
}
