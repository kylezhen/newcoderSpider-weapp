package com.zhao.newcoder.VO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class LoginResult {
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @ApiModelProperty(value = "用户token")
    private String token;
    @ApiModelProperty(value = "用户姓名")
    private String username;
    @ApiModelProperty(value = "状态码")
    private String code;
    @ApiModelProperty(value = "状态描述")
	private String codeDesc;
}