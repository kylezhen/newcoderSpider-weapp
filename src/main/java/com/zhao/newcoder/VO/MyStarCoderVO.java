package com.zhao.newcoder.VO;

import io.swagger.annotations.ApiModelProperty;

public class MyStarCoderVO {
    @ApiModelProperty(value = "面经id")
    private  Long id;
    @ApiModelProperty(value = "标题")
    private  String title;
    @ApiModelProperty(value = "摘要")
    private  String  brief;
    @ApiModelProperty(value = "面经标签")
    private  String  label;
    @ApiModelProperty(value = "面经内容")
    private  String content;
    @ApiModelProperty(value = "面经创建时间")
    private  String createTime;
    @ApiModelProperty(value = "作者")
    private  String author;
    @ApiModelProperty(value = "面经真实url")
    private  String  realUrl;
    @ApiModelProperty(value = "面经纯文字内容，后续用于全文检索")
    private  String txt;
    @ApiModelProperty(value = "面经类型，例如Java,iOS等")
    private  String type;
    @ApiModelProperty(value = "后续可能会用来区分校招实习，社招类型")
    private  Integer phaseId;
}
