package com.zhao.newcoder.VO;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;




@Data
@Document(indexName = "interview", type = "blog", useServerConfiguration = true, createIndex = false)
public class ESCoderVo {

    @Id
    private Long id;

    @Field(type = FieldType.Keyword,analyzer = "ik_max_word")
    private String title;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  String  brief;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  String  label;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  String content;
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private String createTime;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private String author;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  String  realUrl;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  String txt;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  String type;
    @Field(type = FieldType.Keyword,analyzer = "ik_smart")
    private  Integer phaseId;

    private Long viewNum;
}
