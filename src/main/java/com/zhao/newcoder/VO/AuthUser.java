package com.zhao.newcoder.VO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
// import lombok.Getter;
import lombok.NoArgsConstructor;
// import lombok.Setter;
import org.springframework.data.annotation.Id;
// import org.springframework.data.elasticsearch.annotations.Document;
// import org.springframework.data.elasticsearch.annotations.Field;
// import org.springframework.data.elasticsearch.annotations.FieldType;


@Data
@NoArgsConstructor
public class AuthUser  {
    @Id
    @ApiModelProperty("用户主键id")
    private Long id;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("更新时间")
    private String updateTime;
    @ApiModelProperty("状态")
    private Integer status;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("openid")
	private String openid;
}
