package com.zhao.newcoder.VO;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Tags {
    private Integer id;
    private  String name;
}
