package com.zhao.newcoder.Soup;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Splitter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.print.Doc;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Map;

public class SoupUtil {

    public  static  final  String DETAIL_URL="https://www.nowcoder.com//discuss/";
    public static void main(String[] args) throws  Exception{
//        String everypageurl = "https://www.nowcoder.com/discuss/experience?tagId=639";
//        Document document = null;
//        try {
//            document = Jsoup.connect(everypageurl).timeout(500000).get();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String html = document.select("div[class=module-body]").get(0).select("ul[class=column-best-list]").html();
//        System.out.println(html);
//        Elements elements = document.select("li[class=js-nc-wrap-link]");
//        System.out.println(elements);
//        elements.forEach(element -> {
//            System.out.println(element.attr("data-href"));
//        });


//        CloseableHttpClient client = HttpClients.createDefault();
//        HttpGet get = new HttpGet("https://www.nowcoder.com/discuss/experience/json?token=&tagId=639&companyId=0&phaseId=0&order=3&query=&page=1" );
//
//
//
//        HttpResponse response = client.execute(get);
//        //System.out.println(response.getStatusLine().getStatusCode());
//
//        HttpEntity entity = response.getEntity();
//        String content = EntityUtils.toString(entity, "utf-8");
//        JSONObject jsonObject = JSON.parseObject(content);
//        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("discussPosts");
//        for(int i=0;i<jsonArray .size();i++){
//            JSONObject object = jsonArray.getJSONObject(i);
//            String detail_allUrl = DETAIL_URL+object.get("postId");
//            System.out.println(detail_allUrl);
//            Document document = null;
//            try {
//                document = Jsoup.connect(detail_allUrl).timeout(500000).get();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//         String  html = document.select("div[class=post-topic-des nc-post-content]").html();
//            System.out.println(html);
//            //System.out.println(object.get("content").toString().trim());
//        }

        //questionSoup();

        pictureSoup();
    }

   public static void questionSoup() throws Exception{

        Document document = Jsoup.connect("https://www.nowcoder.com/ta/review-frontend").timeout(500000).get();
        Elements elements =document.select("td[class=txt-left]");
        for (Element element:elements){
            String type =element.siblingElements().text();
            String  txt =element.select("a").text();

            String  href = element.select("a").attr("href");
            String tqid = getParam("https://www.nowcoder.com"+href,"tqId");
            Document document1 = Jsoup.connect("https://www.nowcoder.com"+href).timeout(500000).get();
            String question = document1.select("div[class=final-question]").html();

            String html = document1.select("div[class=design-answer-box]").html();
            String answertxt = document1.select("div[class=design-answer-box]").text();
            System.out.println(question);
        }


   }

    public static String getParam(String url, String name) {
        String params = url.substring(url.indexOf("?") + 1, url.length());
        Map<String, String> split = Splitter.on("&").withKeyValueSeparator("=").split(params);
        return split.get(name);
    }


    public  static void pictureSoup() throws Exception{
        Document document = Jsoup.connect("https://www.quanjing.com/hot/k1198.html").timeout(500000).get();
        Elements elements =document.select("a[class=item lazy]");
        for (Element element:elements){
            String src =element.select("img").attr("src");
            System.out.println(src);
        }
    }
}
