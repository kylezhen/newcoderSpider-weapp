package com.zhao.newcoder.Soup;

/**
 * 返回结果
 *
 * @author: ZhouBin
 * @date: 2020/10/9 9:33 上午
*/
public enum ResultCode {
    /**
     * 操作成功！
     */
    SUCCESS(true,200,"操作成功！"),
    /**
     * "操作失败"
     */
    FAIL(false,501,"操作失败"),
    /**
     * "您还未登录"
     */
    UNAUTHENTICATED(false,402,"您还未登录"),
    /**
     * "权限不足"
     */
    UNAUTHORISE(false,403,"权限不足"),
    /**
     * "抱歉，系统繁忙，请稍后重试！"
     */
    SERVER_ERROR(false,500,"抱歉，系统繁忙，请稍后重试！"),

    /**
     * 用户名或密码错误
     */
    MOBILE_OR_PASSWORD_ERROR(false,504,"用户名或密码错误"),
    /**
     * 参数错误
     */
    BIND_EXCEPTION_ERROR(false,505,"参数错误"),
    /**
     * 路径不存在
     */
    NO_HANDLER_FOUND(false,404,"路径不存在"),
    /**
     * 参数不存在
     */
    NO_VALID(false,405,"参数不存在");

    /**
     * 操作是否成功
     */
    boolean success;
    /**
     * 操作代码
     */
    int code;
    /**
     * 提示信息
     */
    String msg;

    ResultCode(boolean success, int code, String msg){
        this.success = success;
        this.code = code;
        this.msg = msg;
    }

    public boolean success() {
        return success;
    }

    public int code() {
        return code;
    }

    public String msg() {
        return msg;
    }
}
