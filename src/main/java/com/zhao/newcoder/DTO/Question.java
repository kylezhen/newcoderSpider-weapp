package com.zhao.newcoder.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "question")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class Question {
    // @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    public Long id;

    @Column(name = "type_title")
    public String typeTitle;

    @Column(name = "type")
    public String type;

    @Column(name = "question")
    public String question;

    @Column(name = "answer")
    public String answer;

    @Column(name = "answer_txt")
    public String answerTxt;

    @Column(name = "create_dt")
    public String createDt;

}
