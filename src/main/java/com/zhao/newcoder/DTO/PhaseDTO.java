package com.zhao.newcoder.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "phase")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class PhaseDTO {
    @Id
    private  Integer id;
    @Column(name = "phase_name")
    private  String phaseName;
}
