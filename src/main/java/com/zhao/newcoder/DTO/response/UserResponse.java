package com.zhao.newcoder.DTO.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息返回
 *
 * @author: ZhouBin
 * @date: 2020/10/9 9:27 上午
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse implements Serializable {

    public  String id;

    public String openid;

    public  String nickName;

    public  String avatarUrl;

    public  String company;

    public String collage;

    public String location;

    public Integer gender;

    public String unionId;

    @NotBlank(message = "请输入用户名")
    public String phoneNumber;

    @NotBlank(message = "请输入密码")
    public String password;

    public String permissions;

    public Date createTime;

    public Date modifyTime;

    public boolean hasPermission(String method){
//        boolean result = false;
//        if(StringUtils.equalsIgnoreCase("get",method)){
//            result = StringUtils.contains(this.permissions,"r");
//        }else{
//            result = StringUtils.contains(this.permissions,"w");
//        }
        return true;
    }
}
