package com.zhao.newcoder.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "auth_user")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer" })
@NoArgsConstructor
public class AuthUser {

    @Column(name = "id")
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "create_time")
    private String createTime;
    @Column(name = "update_time")
    private String updateTime;
    @Column(name = "status")
    private Integer status;
    @Column(name = "password")
    private String password;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;
    @Column(name = "openid")
    private String openid;
}