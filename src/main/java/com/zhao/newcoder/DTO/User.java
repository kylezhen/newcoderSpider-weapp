package com.zhao.newcoder.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zhao.newcoder.DTO.response.UserResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "user")
public class User {
    @Id
    @Column(name = "id")
    public  String id;
    @Column(name = "openid")
    public String openid;
    @Column(name = "nick_name")
    @ApiModelProperty(value = "昵称")
    public  String nickName;
    @Column(name = "avatar_url")
    public  String avatarUrl;
    @Column(name = "company")
    public  String company;
    @Column(name = "collage")
    public String collage;
    @Column(name = "location")
    public String location;
    @Column(name = "gender")
    public Integer gender;
    @Column(name = "union_id")
    public String unionId;
    @Column(name = "phone_number")
    public String phoneNumber;
    @Column(name = "password")
    public String password;
    @Column(name = "permissions")
    public String permissions;
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createTime;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date modifyTime;

    /**
     * 用户信息同步到userResponse
     *
     * @author: ZhouBin
     * @date: 2020/10/9 7:43 下午
     * @param
     * @return com.zhao.newcoder.DTO.response.UserResponse
     */
    public UserResponse buildUserResponse(){
        UserResponse response = new UserResponse();
        BeanUtils.copyProperties(this,response);
        return response;
    }
}
