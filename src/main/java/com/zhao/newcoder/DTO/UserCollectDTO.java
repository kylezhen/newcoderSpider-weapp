package com.zhao.newcoder.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "user_collect")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class UserCollectDTO {
    @Id
    @Column(name = "id")
    public  String id;

    @Column(name = "user_id")
    public String userId;

    @Column(name = "interview_id")
    public Long interviewId;

    @Column(name = "create_time")
    public Date createTime;
}
