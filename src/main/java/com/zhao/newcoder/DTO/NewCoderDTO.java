package com.zhao.newcoder.DTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author zhaozhen
 * @date 2020-07-04
 */
@Entity
@Table(name = "interview")
@Data
@NoArgsConstructor
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class NewCoderDTO {
    @Id
    @Column(name = "id")
    private Long id;

    @Field(type = FieldType.Keyword)
    @Column(name="title")
    private String title;

    @Column(name = "brief")
    private  String  brief;
    @Column(name = "label")
    private  String  label;


    @Column(name = "content")
    private  String content;
    @Column(name="create_time")
    private String createTime;
    @Column(name = "author")
    private String author;
    @Column(name = "real_url")
    private  String  realUrl;
    @Column(name = "txt")
    private  String txt;
    @Column(name = "type")
    private  String type;
    @Column(name = "phase_id")
    private  Integer phaseId;

    @Column(name = "view_num")
    private  Long viewNum;

}
