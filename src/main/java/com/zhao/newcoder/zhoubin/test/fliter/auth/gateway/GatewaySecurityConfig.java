package com.zhao.newcoder.zhoubin.test.fliter.auth.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

@Configuration
@EnableResourceServer
@EnableJpaAuditing
public class GatewaySecurityConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private GatewayWebSecurityExpressionHandler gatewayWebSecurityExpressionHandler;

    @Autowired
    private GatewayAccessDeniedHandler accessDeniedHandler;
    @Autowired
    private GatewayAuthenticationEntryPoint authenticationEntryPoint;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.authenticationEntryPoint(authenticationEntryPoint)
                .accessDeniedHandler(accessDeniedHandler)
                .expressionHandler(gatewayWebSecurityExpressionHandler);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/token/**").permitAll()
//                .anyRequest().authenticated();
        http.addFilterBefore(new GatewayRateLimiFilter(), SecurityContextPersistenceFilter.class)
//                .addFilterBefore(new BasicLoginFilter(),SecurityContextPersistenceFilter.class)
                .addFilterBefore(new GatewayAuditLogFilter(), ExceptionTranslationFilter.class)
                .authorizeRequests()
                .antMatchers("/oauth/**","/user/login","/error","/login","/info").permitAll()
                .anyRequest().access("#permissionService.hasPermission(request,authentication)");
    }

    @Bean
    public AuditorAware<String> auditorAware(){
        return new AuditorAware<String>() {
            @Override
            public Optional<String> getCurrentAuditor() {
                if(SecurityContextHolder.getContext().getAuthentication() ==null){
                    return Optional.ofNullable("null");
                }else{

                    String userId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                    return Optional.ofNullable(userId);
                }
//                ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//                UserResponse response = (UserResponse) servletRequestAttributes.getRequest().getSession().getAttribute("user");
//                String userId = null;
//                if(response!=null){
//                    userId = response.getId();
//                }

            }
        };
    }
}
