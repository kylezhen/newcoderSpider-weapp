package com.zhao.newcoder.zhoubin.test.fliter.auth.gateway;

import com.zhao.newcoder.DTO.log.AuditLog;
import com.zhao.newcoder.Repository.log.AuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 认证失败日志记录
 *
 * @author: ZhouBin
 * @date: 2020/10/14 12:04 下午
*/
@Component
public class GatewayAuthenticationEntryPoint extends OAuth2AuthenticationEntryPoint {
    @Autowired
    private AuditLogRepository auditLogRepository;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        if (authException instanceof AccessTokenRequiredException){
            AuditLog auditLogUpd = (AuditLog) request.getAttribute("auditLog");
            auditLogUpd.setMessage("update log 401");
            auditLogUpd.setStatus(HttpStatus.UNAUTHORIZED.value());
            auditLogRepository.saveAndFlush(auditLogUpd);
        }else {
            AuditLog auditLog = new AuditLog();
            auditLog.setMethod(request.getMethod());
            auditLog.setPath(request.getRequestURI());
            String user = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            auditLog.setUserId(user);
            auditLog.setStatus(HttpStatus.UNAUTHORIZED.value());
            auditLog.setMessage("add log 401");
            auditLogRepository.save(auditLog);
        }
        request.setAttribute("logupdated","yes");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        response.getWriter().write("{\"error\":\"Please login\",\"status\":\""+HttpStatus.UNAUTHORIZED.value()+"\"}");
        response.getWriter().flush();
        return;
//        super.commence(request, response, authException);
    }
}
