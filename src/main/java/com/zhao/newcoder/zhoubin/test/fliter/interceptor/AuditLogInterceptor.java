package com.zhao.newcoder.zhoubin.test.fliter.interceptor;

import com.alibaba.fastjson.JSON;
import com.zhao.newcoder.DTO.log.AuditLog;
import com.zhao.newcoder.Repository.log.AuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 访问日志的记录
 *
 * @author: ZhouBin
 * @date: 2020/10/9 12:03 下午
 */
//@Component
public class AuditLogInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuditLogRepository auditLogRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        AuditLog auditLog = new AuditLog();
        auditLog.setMethod(request.getMethod());
        auditLog.setPath(request.getRequestURI());
        auditLog.setMessage("session:"+ JSON.toJSONString(request.getSession().getAttribute("user")));
        auditLogRepository.save(auditLog);
        request.setAttribute("auditLogId",auditLog.getId());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

        Long auditLogId = (Long) request.getAttribute("auditLogId");
        AuditLog auditLog = auditLogRepository.getOne(auditLogId);
        auditLog.setStatus(response.getStatus());
        auditLogRepository.save(auditLog);
    }
}
