package com.zhao.newcoder.zhoubin.test.fliter.handler;

import com.zhao.newcoder.Soup.Result;
import com.zhao.newcoder.Soup.ResultCode;
import com.zhao.newcoder.exception.CommonException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义的公共异常处理器
 *
 * @author: ZhouBin
 * @date: 2020/10/2 5:29 下午
 */
@RestControllerAdvice
public class BaseExceptionHandler {

//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result<Object> error(HttpServletRequest request, HttpServletResponse response, Exception e){
        e.printStackTrace();
        if(e instanceof CommonException){
            return ((CommonException)e).getResult();
        }else if(e instanceof NoHandlerFoundException){
            return new Result(ResultCode.NO_HANDLER_FOUND,e.getMessage());
        }else if(e instanceof ServletRequestBindingException
                || e instanceof HttpMessageNotReadableException
                || e instanceof HttpRequestMethodNotSupportedException
        ){
            return new Result(ResultCode.BIND_EXCEPTION_ERROR,e.getMessage());
        }else if(e instanceof MethodArgumentNotValidException){
            return new Result(ResultCode.MOBILE_OR_PASSWORD_ERROR,e.getMessage());
        }else{
            return new Result(ResultCode.SERVER_ERROR,e.getMessage());
        }
    }

//    @ExceptionHandler(value = AuthorizationException.class)
//    @ResponseBody
//    public Result error(HttpServletRequest request, HttpServletResponse response, AuthorizationException e){
//        e.printStackTrace();
//        return new Result(ResultCode.UNAUTHORISE);
//    }
//
//    @ExceptionHandler(value = AuthenticationException.class)
//    @ResponseBody
//    public Result error(HttpServletRequest request, HttpServletResponse response, AuthenticationException e){
//        e.printStackTrace();
//        return new Result(e.getMessage(),ResultCode.UNAUTHENTICATED);
//    }
}
