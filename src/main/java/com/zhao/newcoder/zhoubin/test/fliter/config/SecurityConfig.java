package com.zhao.newcoder.zhoubin.test.fliter.config;

import com.zhao.newcoder.DTO.response.UserResponse;
import com.zhao.newcoder.zhoubin.test.fliter.interceptor.AclInterceptor;
import com.zhao.newcoder.zhoubin.test.fliter.interceptor.AuditLogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Optional;

/**
 * 拦截器配置类
 *
 * @author: ZhouBin
 * @date: 2020/10/9 12:11 下午
 */
//@Component
//@EnableJpaAuditing
public class SecurityConfig implements WebMvcConfigurer {

    @Autowired
    private AuditLogInterceptor auditLogInterceptor;
    @Autowired
    private AclInterceptor aclInterceptor;

    /**
     * 注册自定义的拦截器
     *
     * @author: ZhouBin
     * @date: 2020/10/9 7:22 下午
     * @param registry
     * @return void
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(auditLogInterceptor);
        registry.addInterceptor(aclInterceptor);
    }

    /**
     * AuditLog中设置了日志的操作人userId，并注解可@CreatedBy，在这里注入，实现对其注解设置值
     *
     * @author: ZhouBin
     * @date: 2020/10/9 7:23 下午
     * @param
     * @return org.springframework.data.domain.AuditorAware<java.lang.String>
     */
    @Bean
    public AuditorAware<String> auditorAware(){
        return new AuditorAware<String>() {
            @Override
            public Optional<String> getCurrentAuditor() {

                ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                UserResponse response = (UserResponse) servletRequestAttributes.getRequest().getSession().getAttribute("user");
                String userId = null;
                if(response!=null){
                    userId = response.getId();
                }
                return Optional.ofNullable(userId);
            }
        };
    }
}
