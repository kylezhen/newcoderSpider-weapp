package com.zhao.newcoder.zhoubin.test.fliter.auth.gateway;

import com.zhao.newcoder.DTO.log.AuditLog;
import com.zhao.newcoder.Repository.log.AuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO
 *
 * @author: ZhouBin
 * @date: 2020/10/14 1:25 下午
 */
//@Component
public class BasicLoginFilter extends OncePerRequestFilter {

    @Autowired
    private AuditLogRepository auditLogRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(httpServletRequest,httpServletResponse);
        AuditLog auditLogUpd = (AuditLog) httpServletRequest.getAttribute("auditLog");
        if(auditLogUpd!=null){
            auditLogRepository.save(auditLogUpd);
        }
    }
}
