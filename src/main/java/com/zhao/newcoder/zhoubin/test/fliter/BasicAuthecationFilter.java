package com.zhao.newcoder.zhoubin.test.fliter;

import com.lambdaworks.crypto.SCryptUtil;
import com.zhao.newcoder.DTO.User;
import com.zhao.newcoder.Repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * TODO
 * 简单的认证，通过basic的方式，密码加密使用scrypt加密
 * 后期需要改进认证的方式，个人想采用jwt+shiro+redis的认证方式，后面改进
 *
 * @author: ZhouBin
 * @date: 2020/10/9 10:43 上午
 */
//@Component
//@Order(2)
public class BasicAuthecationFilter extends OncePerRequestFilter {
    @Autowired
    private UserRepository userRepository;
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authHandler = httpServletRequest.getHeader("Authorization");
        if(StringUtils.isNotBlank(authHandler)){
            String token64 = StringUtils.substringAfter(authHandler,"Basic ");
            String token = new String(Base64Utils.decodeFromString(token64));
            String[] items = StringUtils.splitByWholeSeparatorPreserveAllTokens(token,":");
            String id = items[0];
            String password = items[1];
            Optional<User> optionalUser = userRepository.findById(id);
            if (optionalUser.isPresent() && SCryptUtil.check(password,optionalUser.get().getPassword())){
                httpServletRequest.getSession().setAttribute("user",optionalUser.get().buildUserResponse());
                httpServletRequest.getSession().setAttribute("temp","yes");

            }
        }
        try{
            filterChain.doFilter(httpServletRequest,httpServletResponse);
        }finally {
            HttpSession session = httpServletRequest.getSession();
            if(session.getAttribute("temp") !=null){
                session.invalidate();
            }
        }
    }
}
