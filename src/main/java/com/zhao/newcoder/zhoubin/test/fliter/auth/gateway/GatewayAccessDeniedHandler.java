package com.zhao.newcoder.zhoubin.test.fliter.auth.gateway;

import com.zhao.newcoder.DTO.log.AuditLog;
import com.zhao.newcoder.Repository.log.AuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 授权失败日志记录
 *
 * @author: ZhouBin
 * @date: 2020/10/14 12:04 下午
*/
@Component
public class GatewayAccessDeniedHandler extends OAuth2AccessDeniedHandler {
    @Autowired
    private AuditLogRepository auditLogRepository;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException authException) throws IOException, ServletException {
        request.setAttribute("logupdated","yes");
        AuditLog auditLogUpd = (AuditLog) request.getAttribute("auditLog");
        auditLogUpd.setStatus(response.getStatus());
        auditLogUpd.setMessage("update log 403");
        auditLogRepository.save(auditLogUpd);

        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType("application/json");
        response.getWriter().write("{\"error\":\"No authority\",\"status\":\""+HttpStatus.FORBIDDEN.value()+"\"}");
        response.getWriter().flush();
        return;
//        super.handle(request, response, authException);
    }
}
