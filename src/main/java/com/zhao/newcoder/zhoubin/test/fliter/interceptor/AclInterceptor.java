package com.zhao.newcoder.zhoubin.test.fliter.interceptor;

import com.alibaba.fastjson.JSON;
import com.zhao.newcoder.DTO.User;
import com.zhao.newcoder.DTO.response.UserResponse;
import com.zhao.newcoder.Soup.Result;
import com.zhao.newcoder.Soup.ResultCode;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TODO
 * 简单的进行授权的拦截器，后期需要改进用户获取以及权限校验的方法
 *
 * @author: ZhouBin
 * @date: 2020/10/9 12:35 下午
 */
//@Component
public class AclInterceptor extends HandlerInterceptorAdapter {

    private String[] permitUrls = new String[]{"/user/login","/login","/info"};

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean result = true;


//        if(!ArrayUtils.contains(permitUrls,request.getRequestURI())){
//            UserResponse user = (UserResponse) request.getSession().getAttribute("user");
//            if (user==null){
//                response.setCharacterEncoding("UTF-8");
//                response.getWriter().write(JSON.toJSONString(new Result(ResultCode.UNAUTHENTICATED)));
//                response.setStatus(HttpStatus.UNAUTHORIZED.value());
//                result = false;
//            }else {
//                String method = request.getMethod();
//                if(!user.hasPermission(method)){
//                    response.setCharacterEncoding("UTF-8");
//                    response.getWriter().write(JSON.toJSONString(new Result(ResultCode.UNAUTHORISE)));
//                    response.setStatus(HttpStatus.FORBIDDEN.value());
//                    result = false;
//                }
//            }
//        }
        return result;
    }
}
