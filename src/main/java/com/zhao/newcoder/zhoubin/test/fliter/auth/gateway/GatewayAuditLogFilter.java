package com.zhao.newcoder.zhoubin.test.fliter.auth.gateway;

import com.zhao.newcoder.DTO.log.AuditLog;
import com.zhao.newcoder.Repository.log.AuditLogRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GatewayAuditLogFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String user = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AuditLog auditLog = new AuditLog();
        auditLog.setMethod(httpServletRequest.getMethod());
        auditLog.setPath(httpServletRequest.getRequestURI());
        auditLog.setMessage("add log for "+user);
        auditLog.setUserId(user);
        httpServletRequest.setAttribute("auditLog",auditLog);

        filterChain.doFilter(httpServletRequest,httpServletResponse);
        if (StringUtils.isBlank((String) httpServletRequest.getAttribute("logupdated"))){
            AuditLog auditLogUpd = (AuditLog) httpServletRequest.getAttribute("auditLog");
            auditLogUpd.setStatus(httpServletResponse.getStatus());
            auditLogUpd.setMessage("update log to success");
            httpServletRequest.setAttribute("auditLog",auditLogUpd);
        }
        System.out.println("request success");
    }
}
