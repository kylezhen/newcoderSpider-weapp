package com.zhao.newcoder.zhoubin.test.fliter.handler;

import com.zhao.newcoder.Soup.Result;
import com.zhao.newcoder.Soup.ResultCode;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO
 *
 * @author: ZhouBin
 * @date: 2020/10/12 12:00 下午
 */
@Controller
public class NotFoundException implements ErrorController {
    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping(value = {"/error"})
    @ResponseBody
    public Result error(HttpServletRequest request) {
        return new Result(ResultCode.NO_HANDLER_FOUND);
    }
}