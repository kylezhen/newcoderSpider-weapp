package com.zhao.newcoder.zhoubin.test.fliter;

import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.RateLimiter;
import com.zhao.newcoder.Soup.Result;
import com.zhao.newcoder.Soup.ResultCode;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO
 * 后面限流应该在网关、熔断器等其他当时实现，以及nginx也可以实现
 * 最简单的限流，引入guava包，定义过滤器
 *
 * @author: ZhouBin
 * @date: 2020/10/9 8:53 上午
 */
//@Component
//@Order(1)
public class RatelimitFilter extends OncePerRequestFilter {

    /**
     * 一秒钟允许访问500次
     */
    private RateLimiter rateLimiter = RateLimiter.create(500);
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (rateLimiter.tryAcquire()){
            filterChain.doFilter(httpServletRequest,httpServletResponse);
        }else {
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
            httpServletResponse.getWriter().write(JSON.toJSONString(new Result(ResultCode.SERVER_ERROR)));
            httpServletResponse.getWriter().flush();
            return;
        }
    }
}
