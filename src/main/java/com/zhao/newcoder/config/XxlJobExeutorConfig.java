package com.zhao.newcoder.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.zhao.newcoder.DTO.NewCoderDTO;
import com.zhao.newcoder.Repository.CoderRepository;
import com.zhao.newcoder.Repository.NewCoderRepository;
import com.zhao.newcoder.VO.ESCoderVo;
import com.zhao.newcoder.VO.Tags;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
public class XxlJobExeutorConfig {

    @Autowired
    NewCoderRepository newCoderRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    CoderRepository coderRepository;
    public static final String DETAIL_URL = "https://www.nowcoder.com/discuss/";

    // Java社招
    public static  final  String JAVA_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=639&companyId=0&phaseId=3&order=3&query=&page=";
    //iOS社招
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=641&companyId=0&phaseId=0&order=3&query=&page=1
    public static  final  String iOS_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=641&companyId=0&phaseId=3&order=3&query=&page=";
    //前端社招
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=644&companyId=0&phaseId=3&order=3&query=&page=1
    public static  final  String WEB_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=644&companyId=0&phaseId=3&order=3&query=&page=";
    //安卓社招等
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=642&companyId=0&phaseId=3&order=3&query=&page=1
    public static final  String Android_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=642&companyId=0&phaseId=3&order=3&query=&page=";

    //大数据面经。包含校招实习
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=733&companyId=0&phaseId=0&order=3&query=&page=1
    public static final String bigdata_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=733&companyId=0&phaseId=0&order=3&query=&page=";
    //测试面经
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=680&companyId=0&phaseId=3&order=3&query=&page=1
    public static final String TEST_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=680&companyId=0&phaseId=3&order=3&query=&page=";

    //C面经
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=640&companyId=0&phaseId=3&order=3&query=&page=1
    public static final String C_CODER_URL ="https://www.nowcoder.com/discuss/experience/json?token=&tagId=640&companyId=0&phaseId=3&order=3&query=&page=";
    //算法面经
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=645&companyId=0&phaseId=3&order=3&query=&page=1
    public static final String ALGORITHM_CODER_URL ="https://www.nowcoder.com/discuss/experience/json?token=&tagId=645&companyId=0&phaseId=3&order=3&query=&page=";

    //数据分析
    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=894&companyId=0&phaseId=3&order=3&query=&page=1
    public static  final  String DATAANALYSIS_CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=894&companyId=0&phaseId=3&order=3&query=&page=";
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("demoJobHandler")
    public ReturnT<String> demoJobHandler(String param) throws Exception {
        return ReturnT.SUCCESS;
    }

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("javaJobHandler")
    public ReturnT<String> javaJobHandler(String param) throws Exception {
            Soup("Java",JAVA_CODER_URL);
            return ReturnT.SUCCESS;
    }

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("iOSJobHandler")
    public ReturnT<String> iOSJobHandler(String param) throws Exception {
        Soup("iOS",iOS_CODER_URL);
        return ReturnT.SUCCESS;
    }
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("AndroidJobHandler")
    public ReturnT<String> AndroidJobHandler(String param) throws Exception {
        Soup("Android",Android_CODER_URL);
        return ReturnT.SUCCESS;
    }
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("testJobHandler")
    public ReturnT<String> testJobHandler(String param) throws Exception {
        Soup("test",TEST_CODER_URL);
        return ReturnT.SUCCESS;
    }
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("webJobHandler")
    public ReturnT<String> webJobHandler(String param) throws Exception {
        Soup("web",WEB_CODER_URL);
        return ReturnT.SUCCESS;
    }
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("bigdataJobHandler")
    public ReturnT<String> bigDataJobHandler(String param) throws Exception {
        Soup("bigdata",bigdata_CODER_URL);
        return ReturnT.SUCCESS;
    }
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("CJobHandler")
    public ReturnT<String> CJobHandler(String param) throws Exception {
        Soup("C",C_CODER_URL);
        return ReturnT.SUCCESS;
    }
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("algorithmJobHandler")
    public ReturnT<String> algorithmJobHandler(String param) throws Exception {
        Soup("algorithm",ALGORITHM_CODER_URL);
        return ReturnT.SUCCESS;
    }


    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("dataAnalysisJobHandler")
    public ReturnT<String> dataAnalysisJobHandler(String param) throws Exception {
        Soup("dataAnalysis",DATAANALYSIS_CODER_URL);
        return ReturnT.SUCCESS;
    }

    public   void  Soup(String type,String coderUrl) throws Exception{
        for (int j=1;j<3;j++){
            CloseableHttpClient client = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(coderUrl+j);
            System.out.println(coderUrl+j);
            HttpResponse response = client.execute(httpGet);

            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity, "utf-8");
            JSONObject jsonObject = JSON.parseObject(content);
            JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("discussPosts");
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                String detail_allUrl = DETAIL_URL + object.get("postId");
                System.out.println(detail_allUrl);
                Document document = null;
                try {
                    document = Jsoup.connect(detail_allUrl).timeout(500000).get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String html = document.select("div[class=post-topic-des nc-post-content]").html();
                NewCoderDTO dto = new NewCoderDTO();
                dto.setId(object.getLong("postId"));
                dto.setAuthor(object.getString("author"));
                dto.setBrief(object.getString("content"));
                dto.setCreateTime(object.getString("createTime"));
                dto.setRealUrl(detail_allUrl);
                dto.setTitle(object.getString("postTitle"));
                dto.setContent(html);
                String txt = document.select("div[class=post-topic-des nc-post-content]").text();
                dto.setTxt(txt);
                dto.setType(type);
                JSONArray jsonArray1 =object.getJSONArray("tags");
                List<Tags> list = jsonArray1.toJavaList(Tags.class);
                String label = list.stream().map(x->x.getName()).collect(Collectors.joining(","));
                dto.setLabel(label);
                dto.setViewNum(0L);
                try {
                    newCoderRepository.save(dto);
                    ESCoderVo vo = new ESCoderVo();
                    BeanUtils.copyProperties(dto,vo);
                    coderRepository.save(vo);
                }catch (Exception e){
                    log.error("保存数据错误",e);
                }

            }

        }
    }
}
