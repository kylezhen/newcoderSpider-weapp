//package com.zhao.newcoder.config;
//
//import com.alibaba.fastjson.JSON;
//import com.zhao.newcoder.DTO.NewCoderDTO;
//import com.zhao.newcoder.Repository.CoderRepository;
//import com.zhao.newcoder.Repository.NewCoderRepository;
//import com.zhao.newcoder.VO.ESCoderVo;
//import com.zhao.newcoder.VO.NewCoderVO;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
//import org.springframework.util.CollectionUtils;
//
//import java.util.List;
//
//@Configuration
//public class ESConfig {
//
//    public void  Es(){
//        //若索引不存在
//        if(!elasticsearchTemplate.indexExists(ESCoderVo.class)){
//            //创建索引
//            elasticsearchTemplate.createIndex(ESCoderVo.class);
//            List<NewCoderDTO> list = newCoderRepository.findAll();
//
//            //将MySQL数据插入到ES
//            if(!CollectionUtils.isEmpty(list)){
//                List<ESCoderVo> esList = JSON.parseArray(JSON.toJSONString(list), ESCoderVo.class);
//                coderRepository.saveAll(esList);
//            }
//        }
//    }
//}
