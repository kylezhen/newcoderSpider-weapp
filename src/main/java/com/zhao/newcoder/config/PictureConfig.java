//package com.zhao.newcoder.config;
//
//import com.zhao.newcoder.DTO.Img;
//import com.zhao.newcoder.Repository.ImgRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.event.EventListener;
//
//@Configuration
//@Slf4j
//public class PictureConfig {
//
//    @Autowired
//    ImgRepository imgRepository;
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void pictureSoup() throws Exception{
//        Document document = Jsoup.connect("https://www.quanjing.com/hot/k1198.html").timeout(500000).get();
//        Elements elements =document.select("a[class=item lazy]");
//        for (Element element:elements){
//            String src =element.select("img").attr("src");
//            Img img = new Img();
//            img.setImgUrl(src);
//            imgRepository.save(img);
//        }
//    }
//}
