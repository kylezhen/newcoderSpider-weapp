//package com.zhao.newcoder.config;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.zhao.newcoder.DTO.NewCoderDTO;
//import com.zhao.newcoder.Repository.NewCoderRepository;
//import com.zhao.newcoder.VO.Tags;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.event.EventListener;
//
//import java.io.IOException;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Configuration
//@Slf4j
//public class TaskConfig {
//
//    public static final String DETAIL_URL = "https://www.nowcoder.com/discuss/";
//    // Java社招
//    //public static  final  String CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=639&companyId=0&phaseId=3&order=3&query=&page=";
//    //iOS社招
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=641&companyId=0&phaseId=0&order=3&query=&page=1
//    //public static  final  String CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=641&companyId=0&phaseId=3&order=3&query=&page=";
//    //前端社招
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=644&companyId=0&phaseId=3&order=3&query=&page=1
//    //public static  final  String CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=644&companyId=0&phaseId=3&order=3&query=&page=";
//    //安卓社招等
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=642&companyId=0&phaseId=3&order=3&query=&page=1
//    //大数据面经。包含校招实习
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=733&companyId=0&phaseId=0&order=3&query=&page=1
//    //测试面经
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=680&companyId=0&phaseId=3&order=3&query=&page=1
//
//    //C面经
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=640&companyId=0&phaseId=3&order=3&query=&page=1
//    //算法面经
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=645&companyId=0&phaseId=3&order=3&query=&page=1
//
//    //数据分析
//    //https://www.nowcoder.com/discuss/experience/json?token=&tagId=894&companyId=0&phaseId=3&order=3&query=&page=1
//    public static  final  String CODER_URL = "https://www.nowcoder.com/discuss/experience/json?token=&tagId=733&companyId=0&phaseId=3&order=3&query=&page=";
//
//    @Autowired
//    NewCoderRepository newCoderRepository;
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void InsertSoupData() throws Exception {
//        for (int j=1;j<2;j++){
//            CloseableHttpClient client = HttpClients.createDefault();
//            HttpGet httpGet = new HttpGet(CODER_URL+j);
//            System.out.println(CODER_URL+j);
//            HttpResponse response = client.execute(httpGet);
//
//            HttpEntity entity = response.getEntity();
//            String content = EntityUtils.toString(entity, "utf-8");
//            JSONObject jsonObject = JSON.parseObject(content);
//            JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("discussPosts");
//            for (int i = 0; i < jsonArray.size(); i++) {
//                JSONObject object = jsonArray.getJSONObject(i);
//                String detail_allUrl = DETAIL_URL + object.get("postId");
//                System.out.println(detail_allUrl);
//                Document document = null;
//                try {
//                    document = Jsoup.connect(detail_allUrl).timeout(500000).get();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                String html = document.select("div[class=post-topic-des nc-post-content]").html();
//                NewCoderDTO dto = new NewCoderDTO();
//                dto.setId(object.getLong("postId"));
//                dto.setAuthor(object.getString("author"));
//                dto.setBrief(object.getString("content"));
//                dto.setCreateTime(object.getString("createTime"));
//                dto.setRealUrl(detail_allUrl);
//                dto.setTitle(object.getString("postTitle"));
//                dto.setContent(html);
//                String txt = document.select("div[class=post-topic-des nc-post-content]").text();
//                dto.setTxt(txt);
//                dto.setType("bigdata");
//                JSONArray jsonArray1 =object.getJSONArray("tags");
//                List<Tags> list = jsonArray1.toJavaList(Tags.class);
//                String label = list.stream().map(x->x.getName()).collect(Collectors.joining(","));
//                dto.setLabel(label);
//                try {
//                    newCoderRepository.save(dto);
//                }catch (Exception e){
//                    log.error("保存数据错误",e);
//                }
//
//            }
//            Thread.sleep(60000);
//
//
//        }
//    }
//}