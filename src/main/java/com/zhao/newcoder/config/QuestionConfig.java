//package com.zhao.newcoder.config;
//
//import com.google.common.base.Splitter;
//import com.zhao.newcoder.DTO.Question;
//import com.zhao.newcoder.Repository.QuestionRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.event.EventListener;
//
//import java.util.Date;
//import java.util.Map;
//
//@Configuration
//@Slf4j
//public class QuestionConfig {
//
//        @Autowired
//        QuestionRepository  questionRepository;
//
//       @EventListener(ApplicationReadyEvent.class)
//       public void  questionSoup() throws Exception{
//           for (int i=1;i<=4;i++){
//               Document document = Jsoup.connect("https://www.nowcoder.com/ta/review-ml?query=&asc=true&order=&tagQuery=&page="+i).timeout(500000).get();
//               Elements elements =document.select("td[class=txt-left]");
//               for (Element element:elements){
//                   Question question = new Question();
//                   question.setTypeTitle("算法/机器学习校招面试题目合集");
//
//                   String type =element.siblingElements().text();
//                   question.setType(type);
//
//                   String  href = element.select("a").attr("href");
//
//                   String tqid = getParam("https://www.nowcoder.com"+href,"tqId");
//                   question.setId(Long.valueOf(tqid));
//                   Document document1 = Jsoup.connect("https://www.nowcoder.com"+href).timeout(500000).get();
//                   String html = document1.select("div[class=design-answer-box]").html();
//                   question.setAnswer(html);
//                   String txt = document1.select("div[class=final-question]").html();
//                   question.setQuestion(txt);
//                   String answerTxt = document1.select("div[class=design-answer-box]").text();
//                   question.setAnswerTxt(answerTxt);
//                   question.setCreateDt(new Date());
//                   try {
//                       questionRepository.save(question);
//                   }catch (Exception e){
//                       log.error("数据保存异常",e);
//                   }
//
//               }
//           }
//
//       }
//
//    public static String getParam(String url, String name) {
//        String params = url.substring(url.indexOf("?") + 1, url.length());
//        Map<String, String> split = Splitter.on("&").withKeyValueSeparator("=").split(params);
//        return split.get(name);
//    }
//}
